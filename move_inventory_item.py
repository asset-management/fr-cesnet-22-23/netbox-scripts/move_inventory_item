from extras.scripts import *
from dcim.models import Device, Location, InventoryItem, Interface


__version__ = "0.1"
__author__ = "Michal Drobný"

# Timeout for the report run
JOB_TIMEOUT = 300

class MoveInventoryItem(Script):

    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Move Inventory Item"
        description = "Move Inventory Item to another device."
        field_order = ["location", "source_device", "inventory_item", "dest_device", "interface"]

    location = ObjectVar(
        model=Location,
        required=False,
        label="Location",
    )
    source_device = ObjectVar(
        model=Device,
        required=True,
        label="Source Device",
        query_params={
            "location_id": "$location",
        }
    )
    inventory_item = ObjectVar(
        model=InventoryItem,
        required=True,
        label="Inventory Item",
        query_params={
            "device_id": "$source_device",
        }
    )
    dest_device = ObjectVar(
        model=Device,
        required=True,
        label="Destination Device",
    )
    interface = ObjectVar(
        model=Interface,
        required=False,
        default=None,
        label="Interface",
        query_params={
            "device_id": "$dest_device",
        }
    )

    def check_inventory_item(self, source_device, inventory_item):
        return inventory_item.device == source_device

    def check_interface(self, dest_device, interface):
        return interface is None or interface.device == dest_device

    def run(self, data, commit):
        source_device = data.get("source_device")
        inventory_item = data.get("inventory_item")
        dest_device = data.get("dest_device")
        interface = data.get("interface")

        if not self.check_inventory_item(source_device, inventory_item):
            self.log_failure(f"[CHECK] The given Inventory Item ({inventory_item.name}) does not belong to the given Source Device ({source_device.name}).")
            return "Error"
        self.log_success(f"[CHECK] The given Inventory Item ({inventory_item.name}) belongs to the given Source Device ({source_device.name}).")

        if not self.check_interface(dest_device, interface):
            self.log_failure(f"[CHECK] The given Interface ({interface.name}) does not belong to the given Destination Device ({dest_device.name}).")
            return "Error"
        self.log_success(f"[CHECK] The given Interface ({interface.name}) belongs to the given Destination Device ({dest_device.name}).")

        component_name = inventory_item.component

        inventory_item.device_id = dest_device.id
        inventory_item.parent = None
        inventory_item.component_id = None
        inventory_item.component_type_id = None
        if interface is not None:
            inventory_item.component_id = interface.id
            inventory_item.component_type_id = 14 # Id of Interface Component
        inventory_item.save()

        self.log_success(f"Inventory Item ({inventory_item.name}) has been moved to Device ({dest_device.name}).")
        
        self.log_info(f"BEFORE:")
        self.log_info(f"- name: {inventory_item.name}")
        self.log_info(f"- device_name: {source_device.name}")
        self.log_info(f"- device_id: {source_device.id}")
        self.log_info(f"- interface: {component_name}")
        self.log_info(f"AFTER:")
        self.log_info(f"- name: {inventory_item.name}")
        self.log_info(f"- device_name: {dest_device.name}")
        self.log_info(f"- device_id: {dest_device.id}")
        self.log_info(f"- interface: {inventory_item.component}")

        return "OK"
