# Move Inventory Item

## Popis
Tento skript je určen pro přesouvání inventárních položek (Inventory Items) mezi různými zařízeními.

## Instalace
- **[Verze 3.4.X a níže]** Skript je nutno vložit do složky scripts, která má nejčastěji tuto cestu netbox/scripts/.
- **[Verze 3.5.X a výše]** Skript je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Scripts -> Add
- Viz https://docs.netbox.dev/en/stable/customization/custom-scripts/

## Výstup
![](/images/move_inventory_item.gif)

## Prerekvizity
1. Vytvořen objekt Device
2. Vytvořen objekt Inventory item

## Postup fungování skriptu
 1. Skript zkontroluje, zdali daná inventární položka patří ke zdrojovému zařízení. Pokud ne, tak skript skončí a nebudou provedeny žádné změny. 
 2. Skript zkontroluje, zdali daný port patří k cílovému zařízení. Pokud ne, tak skončí a nebudou provedeny žádné změny. 
 3. Skript dané inventární položce změní atribut <device>. Nově bude odkazovat na cílové zařízení.
 4. Dále nastaví atributy <parent>, <component> a <component_type_id> na None.
 5. Pokud byl zadán port, jakožto komponenta (Component) dané inventární položky, tak bude nastaven. <component_type_id> bude nastaven napr. na hodnotu 14.
 6. Takto změněná inventární položka bude uložena, čímž se změny aplikují.
 7. Uživatel bude v průběhu informován pomocí logů. 

## Proměnné
**Pro správné fungování skriptu je nutné upravit globální proměnné.**

#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh skriptu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```

## Argumenty

#### Location
 - Nepovinný
 - Tento argument slouží především pro lepší hledání zdrojového zařízení (Source Device).

#### Source Device
 - Povinný
 - Tento argument představuje zařízení, ze kterého se inventární položka bude přesouvat.

#### Inventory Item
 - Povinný
 - Tento argument představuje inventární položku, která má být přesunuta.

#### Destination Device
 - Povinný
 - Tento argument představuje zařízení, na které se bude inventární položka přesouvat.

#### Interface
 - Nepovinný
 - Tento argument představuje port (Interface), ke kterému se má daná inventární položka přiřadit.
 - Pokud zůstane prázdný, tak se inventární položka nepřiřadí žádnému portu (zůstane pouze u cílového zařízení).
